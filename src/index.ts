import { Message } from 'node-nats-streaming';

import { stan } from './nats';
import { getConnection } from './mongo';

stan.on('connect', async () => {
  const client = await getConnection();
  const db = await client.db('diseases');

  stan
    .subscribe(
      'DISEASE_CREATED',
      stan.subscriptionOptions().setDeliverAllAvailable()
    )
    .on('message', async (msg: Message) => {
      const disease: any = JSON.parse(msg.getData() as string);
      await db.collection('diseases').insertOne(disease);
      console.log(`created ${disease}`);
    });

  console.log('sink running');
});
